import React from 'react';
import Link from "next/link";
import Image from "next/image";
import classes from "./thumbsfull.module.scss";

const ThumbFull = () => {
  return (
	<>
        <div className={classes.gridWrapFull}>
			<Link className={classes.thumbsItem} href="/">
				<a>
				<article className={classes.thumbsWrap}>
					<div className={classes.thumbsContent}>
					<h2 className={classes.ItemTitle}>Space Name</h2>
					</div>
					<Image
					src="/images/thumbsfull.png"
					height={900}
					width={1600}
					alt="Thumbs"
					/>
				</article>
				</a>
			</Link>
			<Link className={classes.thumbsItem} href="/">
				<a>
				<article className={classes.thumbsWrap}>
					<div className={classes.thumbsContent}>
					<h2 className={classes.ItemTitle}>Space Name</h2>
					</div>
					<Image
					src="/images/thumbsfull.png"
					height={900}
					width={1600}
					alt="Thumbs"
					/>
				</article>
				</a>
			</Link>
	  
        </div>
    </>
  );
}

export default ThumbFull;
