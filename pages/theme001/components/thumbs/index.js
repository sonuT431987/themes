import React, { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Image from "next/image";
import Link from "next/link";
import classes from "./thumbs.module.scss";
import ThumbListing from "./ThumbListing";
import ThumbContent from "./ThumbContents";
import ThumbFull from "./ThumbFull";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faImage } from "@fortawesome/free-regular-svg-icons";

library.add(faClone, faImage);

import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

const Thumbs = () => {
  const [isCopied, setIsCopied] = useState(false);
  const onCopyText = () => {
    setIsCopied(true);
    setTimeout(() => {
      setIsCopied(false);
    }, 1000);
  };
  const codeSnitted = `0xceB945...Bd3c8D5D`;

  return (
    <>
        <div className={classes.bannerWrap}>
          <div className={classes.bannerImg}>
            <Image
              src="/images/userBanner.png"
              alt="Banner Image"
              width={1900}
              height={360}
            />
			<div className={classes.walletConnect}>
				<button type="button" className="btn btn-success btn-lg">Connect Wallet</button>
			</div>
          </div>
          <div className={classes.userWrap}>
            <Image
              className={classes.profilePicture}
              src="/images/profile.png"
              height={250}
              width={250}
              alt="Thumbs"
            />
            <div className={classes.userInfo}>
              <h2 className={classes.userName}>Deekay Kwon 💎</h2>
              <h3 className={classes.userMore}>@Deekaykwon</h3>
              <h3 className={classes.userId}>
                <CopyToClipboard text={codeSnitted} onCopy={onCopyText}>
                  <div className={classes.copyWrap}>
                    {codeSnitted}{" "}
                    <span className={classes.copyTxt}>
                      {isCopied ? (
                        <span>Copied!</span>
                      ) : (
                        <FontAwesomeIcon icon={faClone} />
                      )}
                    </span>
                  </div>
                </CopyToClipboard>
              </h3>
              <div className={classes.statusWrap}>
                <span>99 Collections</span>
                <span>99 Spaces</span>
                <span>99K Likes</span>
              </div>
              <div className={classes.userDesc}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                dvvindustry. Lorem Ipsum is simply dummy text of the printing
                and typesetting dvvindustry. Lorem Ipsum has been dummy text of
                the printing and typesetting dvvindustry. Lorem Ipsum has been
              </div>
              <div className={classes.socialList}>
                <Link href="/"><a>
                  <FontAwesomeIcon icon={faTwitter} size="2x" />
                </a>
				</Link>
                <Link href="/"><a>
                  <FontAwesomeIcon icon={faInstagram} size="2x" />
                </a>
				</Link>
                <Link href="/"><a>
                  <FontAwesomeIcon icon={faFacebook} size="2x" />
                </a>
				</Link>
              </div>
            </div>
          </div>
        </div>
		<div className="container">
			<div className="tabs-wrapper">
				<Tabs>
					<TabList>
						<Tab>Collections</Tab>
						<Tab>Spaces</Tab>
						<Tab>All Assets</Tab>
					</TabList>
					<TabPanel>
						<ThumbListing />
					</TabPanel>
					<TabPanel>
						<ThumbFull />
					</TabPanel>
					<TabPanel>
						<ThumbContent />
					</TabPanel>
				</Tabs>
			</div>
      	</div>
    </>
  );
};

export default Thumbs;
