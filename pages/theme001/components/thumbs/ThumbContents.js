import React from "react";
import Link from "next/link";
import Image from "next/image";
import classes from "./thumbscontent.module.scss";

const ThumbContent = () => {
  return (
    <>
      <div className={classes.gridWrap}>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>Client teach me</h2>
            <p className={classes.ItemDesc}>
              Lorem ipsum dolor sit amet, consectetur ist at...
            </p>
          </div>
        </article>

        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>Client teach me</h2>
            <p className={classes.ItemDesc}>
              Lorem ipsum dolor sit amet, consectetur ist at...
            </p>
          </div>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>R G B</h2>
            <p className={classes.ItemDesc}>
              Lorem ipsum dolor sit amet, consectetur ist at...
            </p>
          </div>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>Deekay Log-in</h2>
            <p className={classes.ItemDesc}>
              Lorem ipsum dolor sit amet, consectetur ist at...
            </p>
          </div>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>Titles</h2>
            <p className={classes.ItemDesc}>Thumbs Descriptions</p>
          </div>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>Titles</h2>
            <p className={classes.ItemDesc}>Thumbs Descriptions</p>
          </div>
        </article>
		<article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>Titles</h2>
            <p className={classes.ItemDesc}>Thumbs Descriptions</p>
          </div>
        </article>
		<article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
          <div className={classes.thumbsContent}>
            <h2 className={classes.ItemTitle}>Titles</h2>
            <p className={classes.ItemDesc}>Thumbs Descriptions</p>
          </div>
        </article>
      </div>
    </>
  );
};

export default ThumbContent;
