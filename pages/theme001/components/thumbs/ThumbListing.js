import React from "react";
import Link from "next/link";
import Image from "next/image";
import classes from "./thumbs.module.scss";

const ThumbListing = () => {
  return (
    <>
      <div className={classes.gridWrap}>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
        </article>
        <article className={classes.thumbsWrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <Image
                src="/images/thumbs.png"
                height={512}
                width={512}
                alt="Thumbs"
              />
            </a>
          </Link>
        </article>
      </div>
    </>
  );
};

export default ThumbListing;
