import React from 'react';
import Link from "next/link";
import Image from "next/image";
import classes from "./thumbs.module.scss";

const ThumbListing = () => {
  return (
	<>
        <div className={classes.gridwrap}>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <article className={classes.thumbsWrap}>
                <Image
                  src="/images/thumbs.jpg"
                  height={350}
                  width={350}
                  alt="Thumbs"
                />
                <div className={classes.thumbsContent}>
                  <h2 className={classes.ItemTitle}>Titles</h2>
                  <p className={classes.ItemDesc}>Thumbs Descriptions</p>
                </div>
              </article>
            </a>
          </Link>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <article className={classes.thumbsWrap}>
                <Image
                  src="/images/thumbs.jpg"
                  height={350}
                  width={350}
                  alt="Thumbs"
                />
                <div className={classes.thumbsContent}>
                  <h2 className={classes.ItemTitle}>Titles</h2>
                  <p className={classes.ItemDesc}>Thumbs Descriptions</p>
                </div>
              </article>
            </a>
          </Link>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <article className={classes.thumbsWrap}>
                <Image
                  src="/images/thumbs.jpg"
                  height={350}
                  width={350}
                  alt="Thumbs"
                />
                <div className={classes.thumbsContent}>
                  <h2 className={classes.ItemTitle}>Titles</h2>
                  <p className={classes.ItemDesc}>Thumbs Descriptions</p>
                </div>
              </article>
            </a>
          </Link>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <article className={classes.thumbsWrap}>
                <Image
                  src="/images/thumbs.jpg"
                  height={350}
                  width={350}
                  alt="Thumbs"
                />
                <div className={classes.thumbsContent}>
                  <h2 className={classes.ItemTitle}>Titles</h2>
                  <p className={classes.ItemDesc}>Thumbs Descriptions</p>
                </div>
              </article>
            </a>
          </Link>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <article className={classes.thumbsWrap}>
                <Image
                  src="/images/thumbs.jpg"
                  height={350}
                  width={350}
                  alt="Thumbs"
                />
                <div className={classes.thumbsContent}>
                  <h2 className={classes.ItemTitle}>Titles</h2>
                  <p className={classes.ItemDesc}>Thumbs Descriptions</p>
                </div>
              </article>
            </a>
          </Link>
          <Link className={classes.thumbsItem} href="/">
            <a>
              <article className={classes.thumbsWrap}>
                <Image
                  src="/images/thumbs.jpg"
                  height={350}
                  width={350}
                  alt="Thumbs"
                />
                <div className={classes.thumbsContent}>
                  <h2 className={classes.ItemTitle}>Titles</h2>
                  <p className={classes.ItemDesc}>Thumbs Descriptions</p>
                </div>
              </article>
            </a>
          </Link>
        </div>
    </>
  );
}

export default ThumbListing;
