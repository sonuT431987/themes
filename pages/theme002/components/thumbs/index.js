import React, { useState } from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import Image from "next/image";
import Link from "next/link";
import classes from "./thumbs.module.scss";
import ThumbListing from "./ThumbListing";
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClone, faImage } from "@fortawesome/free-regular-svg-icons";

library.add(faClone, faImage);

import {
  faFacebook,
  faTwitter,
  faInstagram,
} from "@fortawesome/free-brands-svg-icons";

const Thumbs = () => {

    const [isCopied, setIsCopied] = useState(false);
    const onCopyText = () => {
            setIsCopied(true);
            setTimeout(() => {
            setIsCopied(false);
        }, 1000);
    };
  const codeSnitted = `0xceB945...Bd3c8D5D`;


  return (
    <>
      <div className="container">
        <div className={classes.bannerWrap}>
		<div className={classes.bannerImg}>
            <Image
              src="/images/userBanner.png"
              alt="Banner Image"
              width={1900}
              height={360}
            />
          </div>
          <div className={classes.userWrap}>
            <Image
              className={classes.profilePicture}
              src="/images/profile.png"
              height={250}
              width={250}
              alt="Thumbs"
            />
            <div className={classes.userInfo}>
              <h2 className={classes.userName}>Deekay Kwon 💎</h2>
              <h3 className={classes.userMore}>@Deekaykwon</h3>
              <h3 className={classes.userId}>
                <CopyToClipboard text={codeSnitted} onCopy={onCopyText}>
                    <div className={classes.copyWrap}>{codeSnitted} <span className={classes.copyTxt}>{isCopied ? <span>Copied!</span> : <FontAwesomeIcon icon={faClone} />}</span></div>
                </CopyToClipboard>
              </h3>
              <div className={classes.statusWrap}>
                <span>99 Collections</span>
                <span>99 Spaces</span>
                <span>99K Likes</span>
              </div>
              <div className={classes.userDesc}>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                dvvindustry. Lorem Ipsum is simply dummy text of the printing
                and typesetting dvvindustry. Lorem Ipsum has been dummy text of
                the printing and typesetting dvvindustry. Lorem Ipsum has been
              </div>
              <div className={classes.socialList}>
                <Link href="/"><a>
                  <FontAwesomeIcon icon={faTwitter} size="2x" />
                </a>
				</Link>
                <Link href="/"><a>
                  <FontAwesomeIcon icon={faInstagram} size="2x" />
                </a>
				</Link>
                <Link href="/"><a>
                  <FontAwesomeIcon icon={faFacebook} size="2x" />
                </a>
				</Link>
              </div>
              <div className={classes.btnGroups}>
                <button
                  type="button"
                  className={`${classes.btnPrimary} ${classes.btn}`}
                >
                  Collections
                </button>
                <button type="button" className={`${classes.btnPrimary} ${classes.btn}`}>
                  Spaces
                </button>
                <button type="button" className={`${classes.btnPrimary} ${classes.btn}`}>
                  All Assets
                </button>
              </div>
            </div>
          </div>
        </div>
        
        <ThumbListing />
      </div>
    </>
  );
};

export default Thumbs;
