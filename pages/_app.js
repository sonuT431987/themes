import React, { useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import '../styles/assets/scss/globals.scss';
import Layout from './components/Layout';


function MyApp({ Component, pageProps }) {

  	return (
	  	<>
			<Layout>
				<Component {...pageProps} />
			</Layout>
		</>
	) 
}

export default MyApp
