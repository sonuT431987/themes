import React from 'react';

const Navigation = () => {
  return (
    <div>
        <nav className='menu'>
			<ul>
				<li><a href="#">My Profile</a></li>
				<li><a href="#">Submit NFT</a></li>
				<li><a href="#">Theme</a></li>
			</ul>
		</nav>
    </div>
  );
}

export default Navigation;
