import React from 'react';

const Header = () => {
  return (
    <>
		<header className="header-set-1128">
			<div className="container">
				<div className="header-row">
					<div className="wallet-wrap">
						<ul>
						<li><a href="#">Wallet ID</a></li>
						<li><a href="#">Disconnect</a></li>
						</ul>	
					</div>
				</div>
			</div>
		</header>
    </>
  );
}

export default Header;
