import React from 'react';
import Image from 'next/image';
import SocialFollow from './SocialFollow';

const Footer = () => {
  return (
    <footer>
		<div className='container'>
			<div className='footer-row'>
				<div className='copyright'>Powered by NFTIFY <Image src='/images/down-arrow.svg' alt='Down Logo' width={50} height={30} /> </div>
				<SocialFollow />
			</div>
		</div>
    </footer>
  );
}

export default Footer;
