import React from 'react';
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import {
    faFacebook,
    faTwitter,
    faInstagram
  } from "@fortawesome/free-brands-svg-icons";


const SocialFollow = () => {
  return (
    <div className='social-container'>
        <span>Social Follow: </span>
        <Link href='/'><a>
            <FontAwesomeIcon icon={faTwitter} size="1x" />
        </a></Link>
        <Link href='/'><a>
            <FontAwesomeIcon icon={faFacebook} size="1x" />
        </a>
		</Link>
        <Link href='/'><a>
            <FontAwesomeIcon icon={faInstagram} size="1x" />
        </a>
		</Link>
    </div>
  );
}

export default SocialFollow;
